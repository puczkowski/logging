Logging

Goal:
the goal of the logging is to provide logging functionality similar to the one that python has.

Features:
- possibility to define contextual loggers
- support different logging targets
- support different logging formats
- support logging to different targets and different formats in the same time

