package main

import (
	"logging/pkg/logging"
)

var Logger = logging.GetLogger("ex1")

func main() {
	logging.Configure(&logging.Config{
		Formatters: map[string]logging.Formatter{
			"default": &logging.JSONFormatter{},
		},
		Handlers: map[string]logging.HandlerConfig{
			"console": {
				Handler:   &logging.ConsoleHandler{},
				Formatter: "default",
				Level:     logging.LevelInfo,
			},
		},
		Root: logging.LoggerConfig{
			Level:    logging.LevelInfo,
			Handlers: []string{"console"},
		},
	})
	Logger.Info("log new crucial info %s", "some info")
}
