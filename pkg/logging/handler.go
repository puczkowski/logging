package logging

import (
	"fmt"
	"os"
)

type Handler interface {
	Handle([]byte) error
}

type ConsoleHandler struct{
	output *os.File
}

func (h *ConsoleHandler) Handle(data []byte) error {
	output := h.output
	if output == nil {
		output = os.Stdout
	}
	_, err := fmt.Fprintln(output, string(data))
	return err
}
