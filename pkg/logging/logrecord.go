package logging

import "time"

//LogRecord is created anytime a message is logged and it's passed to the logger for further processing
type LogRecord struct {
	Message    string
	Line       int
	Filename   string
	LoggerName string
	Level      int
	Time       time.Time
	ack        chan bool
}

func (r *LogRecord) Acknowledge() {
	if r.ack != nil {
		select {
		case r.ack <- true:
		}
	}
}

func (r *LogRecord) WaitForAck() {
	<-r.ack
}
