package logging

import (
	"context"
	"strings"
)

type LogKeep struct {
	config        *Config
	stopRecording context.CancelFunc
	LogInput      chan *LogRecord
}

func newLogKeep() *LogKeep {
	logKeep := &LogKeep{
		LogInput: make(chan *LogRecord),
	}
	logKeep.Configure(&Config{
		Formatters: map[string]Formatter{
			"default": &StandardFormatter{},
		},
		Handlers: map[string]HandlerConfig{
			"console": {
				Handler:   &ConsoleHandler{},
				Formatter: "default",
				Level:     LevelInfo,
			},
		},
		Root: LoggerConfig{
			Level:    LevelInfo,
			Handlers: []string{"console"},
		},
	})
	return logKeep
}

var logKeep = newLogKeep()

func Configure(config *Config) {
	logKeep.Configure(config)
}

func (lk *LogKeep) getLoggersForRecord(record *LogRecord) []*LoggerConfig {
	loggerStrings := strings.Split(record.LoggerName, ".")
	loggers := make([]*LoggerConfig, len(loggerStrings))
	k := 0
	for i, _ := range loggerStrings {
		name := strings.Join(loggerStrings[len(loggerStrings)-i-1:], ".")
		if l, ok := lk.config.Loggers[name]; ok {
			loggers[k] = &l
			k++
		}
	}
	return loggers[:k]
}

func (lk *LogKeep) handlerCanHandle(handler *HandlerConfig, record *LogRecord) bool {
	for _, filterName := range handler.Filters {
		if filter, ok := lk.config.Filters[filterName]; ok {
			if !filter.IsOk(record) {
				return false
			}
		}
	}
	return true
}

func (lk *LogKeep) getHandlers(logger *LoggerConfig, record *LogRecord) []HandlerConfig {
	handlers := make([]HandlerConfig, len(logger.Handlers))
	k := 0
	for _, handlerName := range logger.Handlers {
		if handler, ok := lk.config.Handlers[handlerName]; ok && lk.handlerCanHandle(&handler, record) {
			handlers[k] = handler
			k++
		}
	}
	return handlers[:k]
}

func (lk *LogKeep) log(record *LogRecord) {
	loggers := lk.getLoggersForRecord(record)
	loggers = append(loggers, &lk.config.Root)
	for _, logger := range loggers {
		for _, handler := range lk.getHandlers(logger, record) {
			if formatter, ok := lk.config.Formatters[handler.Formatter]; ok {
				if handler.Level <= record.Level {
					handler.Handler.Handle(formatter.Format(record))
				}
			}
		}
		if !logger.Propagate {
			break
		}
	}
}

func (lk *LogKeep) record(ctx context.Context) {
	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				return
			case record := <-lk.LogInput:
				lk.log(record)
				record.Acknowledge()

			}
		}
	}(ctx)
}

func (lk *LogKeep) Configure(config *Config) {
	lk.config = config
	if lk.stopRecording != nil {
		lk.stopRecording()
	}
	ctx, stopRecording := context.WithCancel(context.Background())
	lk.stopRecording = stopRecording
	lk.record(ctx)
}
