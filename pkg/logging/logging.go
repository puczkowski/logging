package logging

import (
	"fmt"
	"io"
	"os"
	"runtime"
	"time"
)

const (
	LevelDebug = 1000
	LevelInfo  = 2000
	LevelError = 3000
	LevelPanic = 7000
	LevelFatal = 10000
)

var LevelStrings = map[int]string{
	LevelDebug: "DEBUG",
	LevelInfo:  "INFO",
	LevelError: "ERROR",
	LevelPanic: "PANIC",
	LevelFatal: "FATAL",
}

type Filter interface {
	IsOk(*LogRecord) bool
}

type Formatter interface {
	Format(*LogRecord) []byte
}

type LoggerOptions struct {
	Writer    io.Writer
	Level     int
	Formatter Formatter
}

type Logger struct {
	name string
	ack bool
}

func GetLogger(name string) *Logger {
	return &Logger{name: name, ack:true}
}

func (l *Logger) log(level int, msg string, args ...interface{}) {
	_, file, line, _ := runtime.Caller(2)
	msg = fmt.Sprintf(msg, args...)
	record := &LogRecord{
		Message:    msg,
		Line:       line,
		Filename:   file,
		LoggerName: l.name,
		Level:      level,
		Time:       time.Now().UTC(),
		ack:        make(chan bool),
	}
	go func() {
		logKeep.LogInput <- record
	}()
	if l.ack {
		record.WaitForAck()
	}

}

func (l *Logger) Log(level int, msg string, args ...interface{}) {
	l.log(level, msg, args...)
}

func (l *Logger) Debug(msg string, args ...interface{}) {
	l.log(LevelDebug, msg, args...)
}

func (l *Logger) Info(msg string, args ...interface{}) {
	l.log(LevelInfo, msg, args...)
}

func (l *Logger) Error(err error, args ...interface{}) {
	l.log(LevelError, err.Error(), args...)
}

func (l *Logger) ErrorAndPanic(err error, args ...interface{}) {
	l.log(LevelPanic, err.Error(), args...)
	panic(err.Error())
}

func (l *Logger) ErrorAndExit(err error, args ...interface{}) {
	l.log(LevelFatal, err.Error(), args...)
	os.Exit(1)
}

func (l *Logger) LogOnError(err error, args ...interface{}) {
	if err != nil {
		l.log(LevelError, err.Error(), args...)
	}
}

func (l *Logger) LogAndPanicOnError(err error, args ...interface{}) {
	if err != nil {
		l.log(LevelPanic, err.Error(), args...)
		panic(err.Error())
	}
}

func (l *Logger) LogAndExitOnError(err error, args ...interface{}) {
	if err != nil {
		l.log(LevelPanic, err.Error(), args...)
		os.Exit(1)
	}
}
