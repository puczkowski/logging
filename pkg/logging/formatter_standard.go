package logging

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

type StandardFormatter struct{}

func (formatter *StandardFormatter) Format(record *LogRecord) []byte {
	lstring, ok := LevelStrings[record.Level]
	if !ok {
		lstring = strconv.Itoa(record.Level)
	}

	return []byte(fmt.Sprintf(
		"| %s | %s | %s line: %d | %s | %s",
		record.Time.Format(time.RFC3339), record.LoggerName, record.Filename, record.Line, lstring, record.Message),
	)
}


type JSONFormatter struct{}

func (formatter *JSONFormatter) Format(record *LogRecord) []byte {
	lstring, ok := LevelStrings[record.Level]
	if !ok {
		lstring = strconv.Itoa(record.Level)
	}
	r, _ :=	json.Marshal(map[string]interface{}{
		"Time": record.Time.Format(time.RFC3339),
		"Logger": record.LoggerName,
		"Filepath": record.Filename,
		"Line": record.Line,
		"Level": lstring,
		"Message": record.Message,
	})
	return r
}
