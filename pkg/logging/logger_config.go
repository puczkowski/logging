package logging

type HandlerConfig struct {
	Handler   Handler
	Filters   []string
	Formatter string
	Level     int
}

type LoggerConfig struct {
	Level     int
	Handlers  []string
	Propagate bool
}

type Config struct {
	Filters    map[string]Filter
	Formatters map[string]Formatter
	Handlers   map[string]HandlerConfig
	Loggers    map[string]LoggerConfig
	Root       LoggerConfig
}

